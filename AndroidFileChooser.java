//文件浏览界面,代码很容易看懂,没有什么注释
public class FileListActivity extends Activity implements OnItemClickListener{
	
	public static final String TAG = "FileListActivity";
	
	private ListView lvFileList;
	FileListAdapter mFileListAdapter;
	private ArrayList<FileInfo> mFileLists;
	private String mSdcardRootPath;
	private String mLastFilePath;
	private TextView tvFilePath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.filelist);
		
		tvFilePath = (TextView) findViewById(R.id.tvfilepath);
		tvFilePath.setBackgroundColor(Color.GRAY);
		
		mSdcardRootPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		
		lvFileList = (ListView) findViewById(R.id.filelist);
		lvFileList.setEmptyView(findViewById(R.id.tvEmptyHint));
		lvFileList.setOnItemClickListener(this);
		setFileListAdapter(mSdcardRootPath);
		
	}
	
	public void setFileListAdapter(String cuttentFilePath) {
		updateFileItems(cuttentFilePath);
		mFileListAdapter = new FileListAdapter(this, mFileLists);
		lvFileList.setAdapter(mFileListAdapter);
	}
	
	public void updateFileItems(String filePath) {
		mLastFilePath = filePath;
		tvFilePath.setText(mLastFilePath);
		
		if (mFileLists == null) {
			mFileLists = new ArrayList<FileListAdapter.FileInfo>();
		}
		if (!mFileLists.isEmpty()) {
			mFileLists.clear();
		}
		
		File[] files = folderScan(filePath);
		if(files == null) 
			return ;
		
		for (int i = 0; i < files.length; i++) {
			if (files[i].isHidden())
				continue;
			String fileAbsolutePath = files[i].getAbsolutePath();
			String fileName = files[i].getName();
			boolean isDirectory = false;
			if (files[i].isDirectory()) {
				isDirectory = true;
			}
			FileInfo mFileInfo = new FileInfo(fileName, fileAbsolutePath, isDirectory);
			mFileLists.add(mFileInfo);
		}
		if (mFileListAdapter != null) {
			mFileListAdapter.notifyDataSetChanged();
		}
	}
	
	public File[] folderScan(String filePath) {
		File file = new File(filePath);
		File[] files = file.listFiles();
		return files;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		FileInfo fileInfo = (FileInfo) (((FileListAdapter) adapterView.getAdapter()).getItem(position));
		Log.d(TAG, "fileInfo = " + fileInfo);
		if (fileInfo.isDirectory()) {
			updateFileItems(fileInfo.getFilePath());
		} else if (fileInfo.isSupportedFile()) {
			Intent intent = new Intent();
			intent.putExtra(MainActivity.EXTRA_FILE_CHOOSER, fileInfo.getFilePath());
			setResult(RESULT_OK, intent);
			finish();
		} else {
			toast(getString(R.string.open_file_error_format));
		}
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			backKeyPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void backKeyPressed() {
		if (!mLastFilePath.equals(mSdcardRootPath)) {
			File filepath = new File(mLastFilePath);
			String parentPath = filepath.getParent();
			updateFileItems(parentPath);
		} else {
			setResult(RESULT_CANCELED);
			toast(getString(R.string.open_file_none));
			finish();
		}
	}
	
	private void toast(CharSequence hint) {
		Toast.makeText(this, hint, Toast.LENGTH_LONG).show();
	}
}



//加载文件的适配器
public class FileListAdapter extends BaseAdapter {
	
	private ArrayList<FileInfo> mFileList;
	private LayoutInflater mInflater;
	private static ArrayList<String> FILE_SUFFIX = new ArrayList<String>();

	static {
		FILE_SUFFIX.add(".ppt");
		FILE_SUFFIX.add(".pptx");
		FILE_SUFFIX.add(".txt");
		FILE_SUFFIX.add(".xml");
		FILE_SUFFIX.add(".log");
	}
	
	enum FileType{
		FILE, DIRECTORY
	}
	
	public FileListAdapter(Context context, ArrayList<FileInfo> fileList) {
		super();
		this.mFileList = fileList;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mFileList.size();
	}

	@Override
	public Object getItem(int position) {
		return mFileList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder mViewHolder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.filelist_adapter, null);
			mViewHolder = new ViewHolder(convertView);
			convertView.setTag(mViewHolder);
		} else {
			mViewHolder = (ViewHolder) convertView.getTag();
		}
		
		FileInfo mFileInfo = (FileInfo) getItem(position);
		
		mViewHolder.fileName.setText(mFileInfo.getFileName());
		
		if (mFileInfo.isDirectory()) {
			mViewHolder.fileIcon.setImageResource(R.drawable.folder);
			mViewHolder.fileName.setTextColor(Color.GRAY);
		} else if (mFileInfo.isSupportedFile()) {
			mViewHolder.fileIcon.setImageResource(R.drawable.file);
			mViewHolder.fileName.setTextColor(Color.GREEN);
		} else {
			mViewHolder.fileIcon.setImageResource(R.drawable.unknow);
			mViewHolder.fileName.setTextColor(Color.RED);
		}
		
		return convertView;
	}
	
	static class ViewHolder{
		ImageView fileIcon;
		TextView fileName;
		
		public ViewHolder (View view) {
			fileIcon = (ImageView) view.findViewById(R.id.fileicon);
			fileName = (TextView) view.findViewById(R.id.filename);
		}
	}
	
	public static class FileInfo{
		private String fileName;
		private String filePath;
		private FileType fileType;
		
		public FileInfo(String fileName, String filePath, boolean isDirectory){
			this.fileName = fileName;
			this.filePath = filePath;
			this.fileType = isDirectory ? FileType.DIRECTORY : FileType.FILE;
		}
		
		public boolean isDirectory(){
			if(fileType == FileType.DIRECTORY)
				return true ;
			else
				return false ;
		}
		
		public boolean isSupportedFile() {
			if(fileName.lastIndexOf(".") < 0)  //Don't have the suffix 
				return false ;
			String fileSuffix = fileName.substring(fileName.lastIndexOf("."));
			if(!isDirectory() && FILE_SUFFIX.contains(fileSuffix))
				return true ;
			else
				return false ;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}
		
		public String toString() {
			return "FileInfo [fileName=" + fileName + ", filePath=" + filePath + "," +
					" fileType=" + fileType + "]";
		}
	}

}

//布局文件
filelist_adapter.xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="horizontal"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent">
    <ImageView android:id="@+id/fileicon"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
    <TextView android:id="@+id/filename"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center_vertical" />  
</LinearLayout>

//filelist.xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent">
    <TextView android:id="@+id/tvfilepath"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
    <ListView android:id="@+id/filelist"
        android:layout_marginTop="10dip"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
    <TextView
        android:id="@+id/tvEmptyHint"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_centerInParent="true"
        android:gravity="center"
        android:text="@string/empty_hint"
        android:textSize="18sp"
        android:textStyle="bold"
        android:visibility="gone" >
    </TextView>
    
</LinearLayout>
